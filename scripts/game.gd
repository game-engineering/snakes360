extends Node2D

@onready var food_spawner = $FoodSpawner
@onready var food_label = $CanvasLayer/FoodLabel
@onready var player = $Player
@onready var boost_button = $CanvasLayer/BoostButton

const FOOD_SCENE = preload("res://scenes/food.tscn")
const SNAKE_SCENE = preload("res://scenes/snake.tscn")
const ORANGE_BODY = preload("res://assets/orange_body.png")
const ORANGE_HEAD = preload("res://assets/orange_head.png")
const GREY_BODY = preload("res://assets/grey_body.png")
const GREY_HEAD = preload("res://assets/grey_head.png")

const START = "res://scenes/start.tscn"

@onready var snakes = $Snakes
@onready var spawned_food = $SpawnedFood

const DIGGING_ANIMATION = preload("res://scenes/digging_animation.tscn")


func _ready():
	if Globals.selected_snake:
		player.set_snake_data(Globals.selected_snake)
	food_spawner.spawn_food(100)
	for i in range(5):
		add_snake()


func add_snake():
	var pos = Vector2(randi_range(-1500, 1500), randi_range(-1500, 1500))
	var digging = DIGGING_ANIMATION.instantiate()
	digging.position = pos
	digging.animation_finished.connect(func():
		var snake : Snake = SNAKE_SCENE.instantiate()
		snake.ai = true
		snake.body_texture = GREY_BODY
		snake.head_texture = GREY_HEAD
		snake.rotation_degrees = randi_range(0, 360)
		snake.position = pos
		snake.boost_speed = player.boost_speed
		snake.speed = player.speed
		snake.modulate = Color.from_hsv(randf(), 1, 1)
		snake.destroyed.connect(_on_snake_destroyed)
		snake.food = randi_range(0, 151)
		snakes.add_child(snake)
		digging.queue_free()
		)
	snakes.add_child(digging)

func _on_head_food_collected(food):
	food_label.text = str(food)
	Globals.food = food

func _physics_process(_delta):
	if is_instance_valid(player):
		# Changing the direction
		var direction = Input.get_vector("left", "right", "up", "down")
		if direction:
			player.direction_angle = direction.angle()
		if Input.is_action_pressed("boost") or boost_button.is_pressed():
			player.boosting = true
		else:
			player.boosting = false

func _on_mobile_joystick_joystick_moved(direction : Vector2):
	player.direction_angle = direction.angle()


func _on_snake_destroyed(positions):
	for pos in positions:
		for i in range(randi_range(3,13)):
			var food = FOOD_SCENE.instantiate()
			food.global_position = pos + Vector2(randf_range(-40, 40), randf_range(-40, 40))
			spawned_food.add_child(food)
	add_snake()
	Globals.kills += 1



func _on_head_destroyed(_positions):
	get_tree().change_scene_to_file(START)
