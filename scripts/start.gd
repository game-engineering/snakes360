extends CanvasLayer

const GAME = "res://scenes/game.tscn"

@onready var new_game_button = $VBoxContainer2/HBoxContainer/NewGameButton
@onready var new_highscore = $NewHighscore
@onready var new_name_edit = $NewHighscore/VBoxContainer/HBoxContainer/NewNameEdit
@onready var high_score = $VBoxContainer2/HighScore
@onready var snake_selector = $VBoxContainer2/HBoxContainer/SnakeSelector


func _ready():
	snake_selector.set_selected_snake(Globals.selected_snake)
	snake_selector.selection_changed.connect(func(snake): Globals.selected_snake = snake)
	new_highscore.visible = false
	if Globals.food > high_score.get_minimum_score():
		new_game_button.disabled = true
		new_highscore.visible = true
		# Prevent input from key controls
		while Input.is_anything_pressed():
			pass
		new_name_edit.grab_focus()
		new_name_edit.text = high_score.latest_name
		new_name_edit.select_all()
		new_name_edit.caret_column = len(new_name_edit.text)



# also used for LineEdit.text_submitted(new_text)
# therefore the optional parameter
func _on_save_highscore_button_pressed(_new_text=""):
	var new_name = new_name_edit.text.strip_edges()
	if not len(new_name):
		new_name = "Unknown"
	high_score.add_entry({"name": new_name, "food": Globals.food, "kills": Globals.kills})	
	new_highscore.visible = false
	new_game_button.disabled = false


func _on_new_game_button_pressed():
	Globals.food = 0
	Globals.kills = 0
	get_tree().change_scene_to_file(GAME)
