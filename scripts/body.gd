extends PathFollow2D

class_name Body

var target_distance := 0

@onready var collision_body = $CharacterBody2D
@onready var curve : Curve2D = get_parent().curve
@onready var sprite_2d = $CharacterBody2D/Sprite2D
@onready var texture : Texture2D:
	set(value):
		sprite_2d.texture = value
	get:
		return sprite_2d.texture


func _physics_process(_delta):
	# We calculate the progress from the end of the
	# path. See head for more info.
	#print(curve, curve.get_baked_length(), target_distance)
	progress = curve.get_baked_length() - target_distance
	
