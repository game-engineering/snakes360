extends TouchScreenButton

signal joystick_moved

var index = -1

@onready var inner = $Inner
var inner_default_position
var center := Vector2(100,100)

var tween : Tween

func _ready():
	center = texture_normal.get_size() / 2
	inner.position = center
	inner_default_position = center

func _input(event):
	if event is InputEventScreenTouch and event.position.x < 600:
		if event.pressed:
			index = event.index
		else:
			move_to(inner_default_position)
	elif event is InputEventScreenDrag:
		if is_pressed() and event.index == index:
			var moved = (event.position - position - center)
			var dir = moved.normalized()
			if moved.length()<center.x:
				move_to(inner_default_position + moved)
			else:
				move_to(inner_default_position + ( dir * center.x ))
			joystick_moved.emit(dir)
	
func move_to(new_pos):
	if tween:
		tween.kill()
	tween = create_tween()
	tween.tween_property(inner, "position", new_pos, 0.1)
