extends Node2D

signal finished

@export var wait_time := 0.2

@onready var sprite_2d : Sprite2D = $Sprite2D
@onready var timer : Timer = $Timer
@onready var animation_player : AnimationPlayer = $Sprite2D/AnimationPlayer


func _ready():
	sprite_2d.hide()
	timer.wait_time = wait_time
	timer.start()

func _on_animation_player_animation_finished(_anim_name):
	finished.emit()


func _on_timer_timeout():
	sprite_2d.show()
	animation_player.play("explosion")
