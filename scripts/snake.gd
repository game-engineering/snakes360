extends CharacterBody2D

class_name Snake

signal food_collected
signal destroyed

const BODY_SCENE = preload("res://scenes/body.tscn")
const EXPLOSION = preload("res://scenes/explosion.tscn")

@export var speed = 200.0
@export var boost_speed = 400.0

@export var rotational_speed = 5

@export var ai := false

@onready var sprite_2d = $Sprite2D
@onready var texture : Texture2D:
	set(value):
		sprite_2d.texture = value
	get:
		return sprite_2d.texture

@onready var path_2d = $Body/Path2D
@onready var curve : Curve2D = path_2d.curve
@onready var mouth = $Mouth
@onready var food_collector_radius:
	set(value):
		$FoodCollector/CollisionShape2D.shape.radius = value
	get:
		return $FoodCollector/CollisionShape2D.shape.radius

@onready var ray_cast_front = $AI/RayCastFront
@onready var ray_cast_left : RayCast2D = $AI/RayCastLeft
@onready var ray_cast_right : RayCast2D = $AI/RayCastRight
@onready var food_finder = $AI/FoodFinder
@onready var food_finder_timer = $AI/FoodFinder/FoodFinderTimer
@onready var collision_timer = $AI/CollisionTimer

@export var head_texture : Texture2D
@export var body_texture : Texture2D
@export var tail_texture : Texture2D

@onready var rotation_tween : Tween
var target_angle := 0.0
var direction_angle := 0.0

var boosting := false
var killed := false

var food := 0:
	set(value):
		food = value
		food_collected.emit(food)

func add_body_part():
	var body : Body = BODY_SCENE.instantiate()
	# Be careful with path following entities
	# They might show up at the wrong positon
	# until they get first updated.
	body.target_distance = (path_2d.get_child_count() - 1) * 35 + 35
	body.position = Vector2(-100000, -100000)
	body.progress = curve.get_baked_length() - body.target_distance
	
	# Add as first child to put it behind existing
	# body parts
	path_2d.add_child(body)
	path_2d.move_child(body, 0)

	# Adjust looks
	body.modulate = modulate
	body.texture = body_texture
	
	# Do not collide with ourselves
	ray_cast_left.add_exception(body.collision_body)
	ray_cast_right.add_exception(body.collision_body)
	ray_cast_front.add_exception(body.collision_body)
	add_collision_exception_with(body.collision_body)
	
func remove_body_part():
	var body = path_2d.get_child(0)
	path_2d.remove_child(body)
	body.queue_free()

func adjust_length():
	@warning_ignore("integer_division")
	var target_length = 5 + floor(food / 10)
	if target_length > path_2d.get_child_count():
		for i in range(target_length - path_2d.get_child_count()):
			add_body_part()
	elif target_length < path_2d.get_child_count():
		for i in range(path_2d.get_child_count() - target_length):
			remove_body_part()

func _ready():
	texture = head_texture
	for i in range(5):
		add_body_part()
	food_collector_radius = 80

func set_snake_data(data : SnakeData) -> void:
	texture = data.head_texture
	body_texture = data.body_texture
	for body in path_2d.get_children():
		body.texture = body_texture

func _physics_process(delta):
	if killed:
		return
	if ai:
		snake_ai()

	var current_speed = speed
	if boosting:
		current_speed = boost_speed
		
	# Changing the direction
	if direction_angle != target_angle:
		target_angle = direction_angle
		
		# If already turning, kill current animation
		if rotation_tween:				 
			rotation_tween.kill()
		rotation_tween = create_tween()

		# Use angle_difference to get the shortest way to the new direction.
		var angle_diff = angle_difference(rotation, target_angle)
		rotation_tween.tween_property(self, "rotation", rotation + angle_diff, 0.1)
	
	velocity = Vector2.RIGHT.rotated(rotation) * current_speed
	var collision = move_and_collide(velocity*delta)
	if collision:
		destroy()
		return
	
	# Following suit of the parts would be easier if we add new points
	# at the beginning. This, however, would require constant movements of all
	# points in memory: https://en.cppreference.com/w/cpp/container/vector
	# We therefore add at the end and have to calculate based on the length of the
	# path in the body process. The length is precalculated anyways, so this
	# is not a performance issue:
	# https://github.com/godotengine/godot/blob/master/scene/resources/curve.cpp#L899
	
	curve.add_point(position)
	
	# Housekeeping
	adjust_length()

func destroy():
	killed = true
	var positions = []
	positions.append(global_position)
	var explosion
	var delay := 0.0
	for body in path_2d.get_children():
		delay += 0.1
		positions.append(body.global_position)
		explosion = EXPLOSION.instantiate()
		explosion.wait_time = randf_range(0.1 + delay, 0.5 + delay)
		body.add_child(explosion)
		explosion.finished.connect(body.queue_free)
	explosion = EXPLOSION.instantiate()
	explosion.wait_time = 0.1 + delay
	explosion.finished.connect(func():
		destroyed.emit(positions)
		queue_free()
		)
	add_child(explosion)


func snake_ai():
	if not collision_timer.is_stopped():
		return
	if ray_cast_front.is_colliding():
		direction_angle += randf_range(3, 3.3)
		collision_timer.start()
	if ray_cast_left.is_colliding():
		ray_cast_right.enabled = false
		direction_angle -= randf_range(0.3, 1.5)
		collision_timer.start()
	elif not ray_cast_right.enabled:
		ray_cast_right.enabled = true
	if ray_cast_right.is_colliding():
		ray_cast_left.enabled = false
		direction_angle += randf_range(0.3, 1.5)
		collision_timer.start()
	elif not ray_cast_left.enabled:
		ray_cast_left.enabled = true
	if ray_cast_left.enabled and ray_cast_right.enabled and food_finder_timer.is_stopped():
		if food_finder.get_overlapping_bodies():
			var food_center := Vector2.ZERO
			for f in food_finder.get_overlapping_bodies():
				food_center += f.global_position
			food_center /= len(food_finder.get_overlapping_bodies())
			if food_center: # Saveguard against div by zero
				direction_angle = (food_center - global_position).angle()
				food_finder_timer.wait_time = randf_range(0.5, 2)
				food_finder_timer.start()
	


func _on_food_collector_body_shape_entered(_body_rid, body, _body_shape_index, _local_shape_index):
	food += 1
	var tween = create_tween()
	tween.tween_property(body, "global_position", mouth.global_position, 0.1)
	tween.tween_callback(body.queue_free)
