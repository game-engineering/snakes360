extends Node2D


@export var left := -1500
@export var right := 1500
@export var up := -1500
@export var down := 1500

@export var spawn_interval := 5
@export var spawn_amount := 40

@export var food_scene : PackedScene

@onready var timer = $Timer
@onready var foods = $Foods




func _ready():
	timer.wait_time = spawn_interval
	timer.one_shot = false
	timer.connect("timeout", _on_timer_timeout)
	timer.start()

func _on_timer_timeout():
	spawn_food(spawn_amount)

func spawn_food(amount):		
	for i in range(amount):
		var pos = Vector2()
		pos.x += randi_range(left, right)
		pos.y += randi_range(up, down)
		var food : Node2D = food_scene.instantiate()
		food.position += pos
		foods.add_child(food)

