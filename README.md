## Attribution

- Font: https://fonts.google.com/specimen/Tektur
- Background, border, food sprites: https://kenney.nl/assets/top-down-tanks
- Explosion: https://opengameart.org/content/explosion-set-1-m484-games
