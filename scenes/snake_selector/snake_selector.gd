extends HBoxContainer

signal selection_changed(snake: SnakeData)

@export var snakes : Array[SnakeData]

@onready var grid_container = $GridContainer
const SNAKE_BUTTON = preload("res://scenes/snake_selector/snake_button.tscn")

func get_selected_snake():
	for button : SnakeButton in grid_container.get_children():
		if button.is_selected():
			return button.snake_data

func set_selected_snake(snake : SnakeData):
	for button : SnakeButton in grid_container.get_children():
		if button.snake_data == snake:
			return button.select()

func _ready():
	for snake in snakes:
		var button : SnakeButton = SNAKE_BUTTON.instantiate()
		button.snake_data = snake
		button.selected.connect(func(new_snake): selection_changed.emit(new_snake))
		grid_container.add_child(button)
	if grid_container.get_child_count() > 0:
		grid_container.get_child(0).select()			


