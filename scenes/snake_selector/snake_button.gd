class_name SnakeButton
extends MarginContainer

signal selected(snake_data: SnakeData)

@onready var selector_frame = $SelectorFrame
@onready var texture_button = $TextureButton

@export var snake_data : SnakeData:
	set(value):
		snake_data = value
		if texture_button:
			texture_button.texture_normal = value.head_texture
			
func select():
	if selector_frame.visible:
		return
	for button : SnakeButton in get_tree().get_nodes_in_group("snakebuttons"):
		button.selector_frame.hide()
	selector_frame.show()
	selected.emit(snake_data)
			
func is_selected():
	return selector_frame.visible

func _ready():
	add_to_group("snakebuttons")
	snake_data = snake_data
	texture_button.pressed.connect(select)
